<?php

namespace Nuwave\Lighthouse\Console;

use Nuwave\Lighthouse\GraphQL;
use Illuminate\Console\Command;
use GraphQL\Utils\SchemaPrinter;

class PrintSchemaCommand extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = '
        lighthouse:print-schema {--W|write : Write the output to a file}
    ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Compile the final GraphQL schema and print the result.';

    /**
     * Execute the console command.
     *
     * @param  \Nuwave\Lighthouse\GraphQL  $graphQL
     * @return void
     */
    public function handle(GraphQL $graphQL): void
    {
        $schema = SchemaPrinter::doPrint($graphQL->prepSchema());
        file_put_contents('schema.graphql', $schema);
    }
}
